package es.amazon.selenium.page;

import es.amazon.selenium.utils.ElementUtils;
import es.amazon.selenium.utils.JavascriptUtils;
import java.util.List;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.html5.SessionStorage;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.remote.html5.RemoteSessionStorage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class NewReleasesPage {

  @Autowired
  private RemoteWebDriver driver;

  @Autowired
  private JavascriptUtils javascriptUtils;

  @Autowired
  private ElementUtils elementUtils;

  private static final String NEW_VIDEOGAMES_RELEASES_LINK_XPATH = "//a[contains(@href,'/new-releases/videogames/ref=zg_bsnr_nav_0')]";

  private static final String NEW_RELEASE_ITEM_PRICE_CLASS = "p13n-sc-price";

  private static final String NEW_RELEASE_ITEM_CLASS = "zg_itemImmersion";

  private static final String NEW_RELEASE_ITEM_NAME_CLASS = "a-link-normal";

  public void clickNewVideogamesReleasesLink() {
    javascriptUtils.clickToElement(getNewVideogamesReleasesLink());
  }

  public void clickLowestPriceNewReleaseItem() {
    int count = getNewReleasesItemsDisplayed().size();
    Double itemprice = null;
    Integer itemposition = null;
    for(int i = 0; i < count; i++) {
      int xpathvalue = i + 1;
      if(elementUtils.existsElement(By.xpath("//div[contains(@class,'"+NEW_RELEASE_ITEM_CLASS+"')]["+ xpathvalue +"]/*/*/*/*/span[contains(@class,'"+NEW_RELEASE_ITEM_PRICE_CLASS+"')]"),0)) {
        double price = getDesiredNewReleasedItemPrice(i);
        if (itemprice == null || price < itemprice) {
          itemprice = price;
          itemposition = i;
        }
      }
    }
    javascriptUtils.clickToElement(getDesiredNewReleasedItem(itemposition));
  }

  private Double getDesiredNewReleasedItemPrice(int item) {
    String value = getNewReleasesItemsDisplayed().get(item).findElement(By.className(NEW_RELEASE_ITEM_PRICE_CLASS)).getText();
    String valueWithoutEUR = value.replaceAll("[EUR | €]", "");
    String valueWithoutDots = valueWithoutEUR.replaceAll("\\.","");
    String valueChangeCommaToDot = valueWithoutDots.replaceFirst(",",".");
    return Double.parseDouble(valueChangeCommaToDot);
  }

  private WebElement getDesiredNewReleasedItem(int item) {
    return getNewReleasesItemsDisplayed().get(item).findElement(By.className(NEW_RELEASE_ITEM_NAME_CLASS));
  }

  private WebElement getNewVideogamesReleasesLink() {
    return driver.findElement(By.xpath(NEW_VIDEOGAMES_RELEASES_LINK_XPATH));
  }

  private List<WebElement> getNewReleasesItemsDisplayed() {
    return driver.findElements(By.className(NEW_RELEASE_ITEM_CLASS));
  }

}
