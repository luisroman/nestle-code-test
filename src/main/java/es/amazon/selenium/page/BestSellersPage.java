package es.amazon.selenium.page;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class BestSellersPage {

  @Autowired
  private RemoteWebDriver driver;

  private static final String NEW_RELEASES_LINK_XPATH = "//a[contains(@href,'/new-releases/ref=zg_bs_tab')]";

  public void clickNewReleasesLink() {
    getNewReleasesLink().click();
  }

  private WebElement getNewReleasesLink() {
    return driver.findElement(By.xpath(NEW_RELEASES_LINK_XPATH));
  }

}
