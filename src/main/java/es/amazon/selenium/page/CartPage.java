package es.amazon.selenium.page;

import es.amazon.selenium.utils.JavascriptUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class CartPage {

  @Autowired
  private RemoteWebDriver driver;

  @Autowired
  private JavascriptUtils javascriptUtils;

  private static final String CHECKOUT_BUTTON_ID = "hlb-ptc-btn-native";

  public void clickCheckoutButton() {
    javascriptUtils.clickToElement(getCheckoutButton());
  }

  private WebElement getCheckoutButton() {
    return driver.findElement(By.id(CHECKOUT_BUTTON_ID));
  }

}
