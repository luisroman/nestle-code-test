package es.amazon.selenium.page;

import es.amazon.selenium.utils.JavascriptUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class ItemDetailPage {

  @Autowired
  private RemoteWebDriver driver;

  @Autowired
  private JavascriptUtils javascriptUtils;

  private static final String ADD_TO_CART_BUTTON_ID = "add-to-cart-button";

  public void clickAddToCartButton() {
    javascriptUtils.clickToElement(getAddToCartButton());
  }

  private WebElement getAddToCartButton() {
    return driver.findElement(By.id(ADD_TO_CART_BUTTON_ID));
  }

}
