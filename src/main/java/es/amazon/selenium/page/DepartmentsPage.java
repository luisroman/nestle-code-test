package es.amazon.selenium.page;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class DepartmentsPage {

  @Autowired
  private RemoteWebDriver driver;

  private static final String BEST_SELLERS_LINK_HREF = "//*[contains(@href,'/gp/bestsellers/ref=sv__0')]";

  public void clickBestSellersLink() {
    getBestSellersLink().click();
  }

  private WebElement getBestSellersLink() {
    return driver.findElement(By.xpath(BEST_SELLERS_LINK_HREF));
  }

}
