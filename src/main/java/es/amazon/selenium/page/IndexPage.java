package es.amazon.selenium.page;

import es.amazon.selenium.utils.JavascriptUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class IndexPage {

  @Autowired
  private RemoteWebDriver driver;

  @Autowired
  private String mainUrl;

  @Autowired
  private JavascriptUtils javascriptUtils;

  private static final String DEPARTMENTS_LINK_ID = "nav-link-shopall";

  public void clickDepartmentsLink() {
    javascriptUtils.clickToElement(getDeparmentsLink());
  }

  public void cleanCookies() {
    driver.manage().deleteAllCookies();
  }

  public void goToMainUrl() {
    driver.get(mainUrl);
  }

  private WebElement getDeparmentsLink() {
    return driver.findElement(By.id(DEPARTMENTS_LINK_ID));
  }

}
