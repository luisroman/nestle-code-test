package es.amazon.selenium.page;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class SignInPage {

  @Autowired
  private RemoteWebDriver driver;

  private static final String SIGN_IN_BUTTON_ID = "signInSubmit";

  public boolean isSignInButtonDisplayed() {
    return getSignInButton().isDisplayed();
  }

  private WebElement getSignInButton() {
    return driver.findElement(By.id(SIGN_IN_BUTTON_ID));
  }

}
