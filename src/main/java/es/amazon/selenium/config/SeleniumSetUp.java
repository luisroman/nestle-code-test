package es.amazon.selenium.config;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.TimeUnit;
import javax.annotation.PreDestroy;
import java.util.Locale;

import es.amazon.selenium.utils.FileUtils;
import org.openqa.selenium.Platform;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.safari.SafariDriver;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

@Configuration
public class SeleniumSetUp {

  private RemoteWebDriver mainDriver = null;

  //Starts by default with Chrome Driver
  @Bean
  @Profile("Chrome")
  public RemoteWebDriver chromeDriver() {
    return buildRemoteWebDriver(browser.CHROME);
  }

  //Firefox 53 version and superior
  @Bean
  @Profile("Firefox")
  public RemoteWebDriver firefoxDriver() {
    return buildRemoteWebDriver(browser.FIREFOX);
  }

  @Bean
  @Profile("Safari")
  public RemoteWebDriver safariDriver() {
    return buildRemoteWebDriver(browser.SAFARI);
  }

  @Bean
  @Profile("IE11")
  public RemoteWebDriver internetExplorerDriver() {
    return buildRemoteWebDriver(browser.IE11);
  }

  //Edge version supported: 15.15063
  @Bean
  @Profile("Edge")
  public RemoteWebDriver edgeDriver() {
    return buildRemoteWebDriver(browser.EDGE);
  }

  @Bean
  @Profile("Grid")
  public RemoteWebDriver gridDriver() {
    //61.0.3163.100
    String browser = FileUtils.readFile(SeleniumSetUp.class.getResource("/gridbrowser.txt").getPath());
    String url = FileUtils.readFile(SeleniumSetUp.class.getResource("/gridurl.txt").getPath());
    System.out.println(browser);
    System.out.println(url);
    DesiredCapabilities capabilities = new DesiredCapabilities();
    capabilities.setBrowserName(browser);
    try {
      mainDriver = new RemoteWebDriver(new URL(url),capabilities);
    } catch (MalformedURLException e) {
      e.printStackTrace();
    }
    mainDriver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
    mainDriver.manage().timeouts().pageLoadTimeout(20, TimeUnit.SECONDS);
    mainDriver.get(mainUrl());
    return mainDriver;
  }

  @Bean
  public String mainUrl() {
    return "https://www.amazon.es";
  }

  private RemoteWebDriver buildRemoteWebDriver(browser selectBrowser) {
    switch (selectBrowser) {
      case CHROME:
        System.setProperty("webdriver.chrome.driver", getChromeDriverPath());
        mainDriver = new ChromeDriver();
        break;
      case FIREFOX:
        System.setProperty("webdriver.gecko.driver", getGeckoDriverPath());
        mainDriver = new FirefoxDriver();
        break;
      case SAFARI:
        mainDriver = new SafariDriver();
        break;
      case IE11:
        System.setProperty("webdriver.ie.driver", getInternetExplorerDriverPath());
        mainDriver = new InternetExplorerDriver();
        break;
      case EDGE:
        System.setProperty("webdriver.edge.driver", getEdgeDriverPath());
        mainDriver = new EdgeDriver();
        break;
    }
    mainDriver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
    mainDriver.manage().timeouts().pageLoadTimeout(20, TimeUnit.SECONDS);
    mainDriver.get(mainUrl());
    return mainDriver;
  }

  private enum browser {
    CHROME, FIREFOX, SAFARI, IE11, EDGE
  }

  @PreDestroy
  public void quit() {
    mainDriver.quit();
  }

  private String getChromeDriverPath() {
      String OS = System.getProperty("os.name", "generic").toLowerCase(Locale.ENGLISH);
        if ((OS.contains("mac")) || (OS.contains("darwin"))) {
            return SeleniumSetUp.class.getResource("/drivers/chromedriver-mac").getPath();
        } else if (OS.contains("win")) {
            return SeleniumSetUp.class.getResource("/drivers/chromedriver-win32.exe").getPath();
        } else if (OS.contains("nux")) {
            return SeleniumSetUp.class.getResource("/drivers/chromedriver-linux32").getPath();
        } else {
            return null;
        }
  }

  private String getGeckoDriverPath() {
      String OS = System.getProperty("os.name", "generic").toLowerCase(Locale.ENGLISH);
        if ((OS.contains("mac")) || (OS.contains("darwin"))) {
            return SeleniumSetUp.class.getResource("/drivers/geckodriver-mac").getPath();
        } else if (OS.contains("win")) {
            return SeleniumSetUp.class.getResource("/drivers/geckodriver-win32.exe").getPath();
        } else if (OS.contains("nux")) {
            return SeleniumSetUp.class.getResource("/drivers/geckodriver-linux32").getPath();
        } else {
            return null;
        }
  }

  private String getInternetExplorerDriverPath() {
    return SeleniumSetUp.class.getResource("/drivers/IEDriverServer-32.exe").getPath();
  }

  private String getEdgeDriverPath() {
    return SeleniumSetUp.class.getResource("/drivers/MicrosoftWebDriver.exe").getPath();
  }

}
