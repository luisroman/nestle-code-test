package es.amazon.selenium.utils;

import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class ElementUtils {


  @Autowired
  private RemoteWebDriver driver;

  public boolean existsElement(By by) {
    try {
      return driver.findElement(by).isDisplayed();
    } catch (NoSuchElementException e) {
      return false;
    }
  }

  public boolean existsElement(By by, Integer sec) {
    turnOffImplicitWaits(sec);
    boolean result = existsElement(by);
    turnOnImplicitWaits();
    return result;
  }

  private void turnOffImplicitWaits(int sec) {
    driver.manage().timeouts().implicitlyWait(sec, TimeUnit.SECONDS);
  }

  private void turnOnImplicitWaits() {
    driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
  }

}
