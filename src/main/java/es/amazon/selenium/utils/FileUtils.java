package es.amazon.selenium.utils;

import java.io.BufferedReader;
import java.io.FileReader;

public class FileUtils {

    public static String readFile(String url) {
        BufferedReader bufferedReader;
        String line = null;
        try {
            bufferedReader = new BufferedReader(new FileReader(url));
            line = bufferedReader.readLine();
        } catch (Exception e) {
            //
        }
        return line;
    }
}
