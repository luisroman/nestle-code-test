package es.amazon.selenium.cucumber.test;

import cucumber.api.CucumberOptions;
import org.junit.runner.RunWith;
import cucumber.api.junit.Cucumber;

@RunWith(Cucumber.class)
@CucumberOptions(features = "classpath:features/", glue = "es.amazon.selenium.cucumber.steps")
public class CucumberTest {

}
