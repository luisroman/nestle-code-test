package es.amazon.selenium.cucumber.steps;

import cucumber.api.java.en.And;
import es.amazon.selenium.page.CartPage;
import org.springframework.beans.factory.annotation.Autowired;

public class CartPageSteps extends BaseSteps {

  @Autowired
  private CartPage cartPage;

  @And("^I click to the Cart Page's Checkout Button$")
  public void i_Click_To_The_Cart_Page_Checkout_Button() {
    cartPage.clickCheckoutButton();
  }

}
