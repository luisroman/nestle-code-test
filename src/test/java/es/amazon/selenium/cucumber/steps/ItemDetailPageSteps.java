package es.amazon.selenium.cucumber.steps;

import cucumber.api.java.en.When;
import es.amazon.selenium.page.ItemDetailPage;
import org.springframework.beans.factory.annotation.Autowired;

public class ItemDetailPageSteps extends BaseSteps {

  @Autowired
  private ItemDetailPage itemDetailPage;

  @When("^I click to the Add to Cart Button$")
  public void i_Click_To_The_Add_To_Cart_Button() {
    itemDetailPage.clickAddToCartButton();
  }

}
