package es.amazon.selenium.cucumber.steps;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import es.amazon.selenium.page.NewReleasesPage;
import org.springframework.beans.factory.annotation.Autowired;

public class NewReleasesPageSteps extends BaseSteps {

  @Autowired
  private NewReleasesPage newReleasesPage;

  @And("^I go to the New Videogames Releases Page$")
  public void i_Click_In_The_New_Videogames_Releases_Link() {
    newReleasesPage.clickNewVideogamesReleasesLink();
  }

  @And("^I click in the New Release Item with the Lowest Price$")
  public void i_Click_In_The_New_Release_Item_With_The_Lowest_Price() {
    newReleasesPage.clickLowestPriceNewReleaseItem();
  }

}
