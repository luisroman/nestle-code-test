package es.amazon.selenium.cucumber.steps;

import es.amazon.selenium.config.SpringConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;

@ContextConfiguration
@SpringBootTest(classes = SpringConfiguration.class)
@TestExecutionListeners(DependencyInjectionTestExecutionListener.class)
public class BaseSteps {

}
