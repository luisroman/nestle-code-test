package es.amazon.selenium.cucumber.steps;

import static org.junit.Assert.assertTrue;

import cucumber.api.java.en.Then;
import es.amazon.selenium.page.SignInPage;
import org.springframework.beans.factory.annotation.Autowired;

public class SignInPageSteps extends BaseSteps {

  @Autowired
  private SignInPage signInPage;

  @Then("^Sign In Page is displayed$")
  public void sign_In_Page_Is_Displayed() {
    assertTrue(signInPage.isSignInButtonDisplayed());
  }

}
