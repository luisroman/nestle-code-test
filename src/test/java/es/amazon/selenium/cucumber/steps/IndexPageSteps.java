package es.amazon.selenium.cucumber.steps;

import cucumber.api.java.After;
import cucumber.api.java.en.Given;
import es.amazon.selenium.page.BestSellersPage;
import es.amazon.selenium.page.DepartmentsPage;
import es.amazon.selenium.page.IndexPage;
import org.springframework.beans.factory.annotation.Autowired;


public class IndexPageSteps extends BaseSteps {

  @Autowired
  private IndexPage indexPage;

  @Autowired
  private BestSellersPage bestSellersPage;

  @Autowired
  private DepartmentsPage departmentsPage;

  @Given("^I am in the New Releases Page$")
  public void i_Am_In_The_New_Releases_Page() {
    indexPage.clickDepartmentsLink();
    departmentsPage.clickBestSellersLink();
    bestSellersPage.clickNewReleasesLink();
  }

  @After
  public void backToMainUrl() {
    indexPage.cleanCookies();
    indexPage.goToMainUrl();
  }


}
