Feature: Amazon Spain New Release Item Page

  Scenario: Add a New Videogame Release Item with the Lowest Price to the basket without Login and try a Checkout
    Given I am in the New Releases Page
    And I go to the New Videogames Releases Page
    And I click in the New Release Item with the Lowest Price
    When I click to the Add to Cart Button
    And I click to the Cart Page's Checkout Button
    Then Sign In Page is displayed
