# Amazon-Selenium (Nestle Test)

*To build this project I used the technology above:*

- Java
- Spring Boot
- Selenium
- Cucumber

*Where to find the .feature file:*

You can find the .feature file with the test in /src/test/resources/features

*How to launch the test:*

You can launch the test for example opening the project in IntelliJ and Run the Project, this will open a Chrome browser where the test will start (If you want you can choose the browser to launch between Chrome, Firefox, IE11 and Edge)

You have some ways to choose the browser you want to open:

- Modifying the POM to launch the test with the desired browser using activeToDefault=true in the profile you want

- Modifying the BaseSteps.java and put the annotation @ActiveProfiles("Here goes the name of the profile of the desired browser")

- Or launching the test in the terminal with the command "mvn test -Dspring.profiles.active=Name of profile to launch"

*Supported browsers:*

- Chrome

- Firefox (Version 53 and superior)

- Internet Explorer *This test was created in a macOS Sierra, so it's configured to be launched with it, but not tried

- Edge 15.15063 *This test was created in a macOS Sierra, so it's configured to be launched with it, but not tried

*Not supported browsers:*

- Safari: Not supported in this test because Safari 10 doesn't accept implicitlyWait and the test fails when there's a transition between pages, the only way to fix this is using a Webdriver wait in each element to be called in the test, it would be better that Apple fix this problem, but if Safari is one of the browsers wanted to test, is possible to do it using Webdriver wait